#!/usr/bin/bash
# Functions
PSQL_VER="15"
PSQL_USER="keycloak"
PSQL_PWD="keycloak"
PSQL_DBNAME="keycloak"

install_postgresql_repo() {
    dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
}

install_postgresql_start(){
    dnf -qy module disable postgresql
    dnf install -y postgresql${PSQL_VER}-server
    /usr/pgsql-${PSQL_VER}/bin/postgresql-${PSQL_VER}-setup initdb
    systemctl enable postgresql-${PSQL_VER}
    systemctl start postgresql-${PSQL_VER}
}

create_user_database(){
    sudo -u postgres psql -c "create database ${PSQL_DBNAME};"
    sudo -u postgres psql -c "create user ${PSQL_USER} role postgres password '${PSQL_PWD}';"
    sudo -u postgres psql -c "grant ALL privileges on database ${PSQL_USER} to ${PSQL_DBNAME};"
}

configure_postgresql(){
    echo "listen_addresses = '*'" >> /var/lib/pgsql/${PSQL_VER}/data/postgresql.conf
    echo "host    thor     all             192.168.0.1/16            md5" >> /var/lib/pgsql/${PSQL_VER}/data/pg_hba.conf
}

configure_firewall() {
    systemctl enable --now firewalld
    firewall-cmd --perm --add-port=5432/tcp
    firewall-cmd --reload
}
restart_postgresql(){
    systemctl restart postgresql-${PSQL_VER}
}

# Let's Go

install_postgresql_repo
install_postgresql_start
create_user_database
configure_postgresql
configure_firewall
restart_postgresql